"""
Factory for the different types of repositories.
"""

def create_repository(name, settings):
    """Creates a repository from its name and settings. The settings
    is a dictionary where the keys are different for every type of repository.
    See each repository for details on the required settings."""
    if name == 'azuretablestorage':
        from .azuretablestorage import Repository
    elif name == 'mongodb':
        from .mongodb import Repository
    elif name == 'memory':
        from .memory import Repository
    else:
        raise ValueError('Unknown repository.')

    return Repository(settings)

def getData(fileContent):
    linesArray = fileContent.readlines()   
    lines = [ int(item.decode("utf-8").split(';')[0]) for item in linesArray ]
    dataSet = [lines,lines,lines,lines] 
    return dataSet
    # with open(name) as f: 
    

# def _load_samples_json():
#     """Loads polls from samples.json file."""
#     samples_path = path.join(path.dirname(__file__), 'samples.json')
#     with open(samples_path, 'r') as samples_file:
#         return