"""
Package for the models.
"""

from os import path
import json

class DataSet(object):
    """A poll object for use in the application views and repository."""
    def __init__(self, key=u''):
        """Initializes the poll."""
        self.key = key
        self.recording = []

class Recording(object):
    """A poll choice object for use in the application views and repository."""
    def __init__(self, samples=[], key=u''):
        """Initializes the poll choice."""
        self.key = key
        self.samples = samples

class DataSetNotFound(Exception):
    """Exception raised when a poll/choice object couldn't be retrieved from
    the repository."""
    pass
