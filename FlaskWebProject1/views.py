"""
Routes and views for the flask application.
"""

from datetime import datetime

from flask import render_template, redirect, request

from FlaskWebProject1 import app,datas,files
from FlaskWebProject1.models.factory import create_repository
from FlaskWebProject1.models.factory import getData
from FlaskWebProject1.settings import REPOSITORY_NAME, REPOSITORY_SETTINGS, PATH_SETTING

import subprocess
import os

repository = create_repository(REPOSITORY_NAME, REPOSITORY_SETTINGS)


# path = PATH_SETTING;

# ---- Flassk funktions ----

@app.route('/')
@app.route('/home')
def home():
    """Renders the home page, with selection of input data."""
    return render_template(
        'index.html',
        title='Select input data',
        fileList=files,
    )

@app.route('/contact')
def contact():
    """Renders the contact page."""
    return render_template(
        'contact.html',
        title='Contact',
        year=datetime.now().year,
    )

@app.route('/editData')
def editData():
    global datas
    """Renders the editData page."""
    return render_template(
        'editData.html',
        title= 'edit data',
        year= datetime.now().year,
        datas= datas,
    )

@app.route('/upload', methods=['POST'])
def uploadData():
    global datas
    """Renders the editData page."""
    # for file in request.files :
    file = request.files['file']
    files.append(file.filename)
    datas = getData(file)
    # datas = data;
    return editData()


@app.route('/about')
def about():
    """Renders the about page."""
    return render_template(
        'about.html',
        title='About',
        year=datetime.now().year,
        repository_name=repository.name,
    )

@app.route('/seed', methods=['POST'])
def seed():
    """Seeds the database with sample polls."""
    repository.add_sample_polls()
    return redirect('/')

@app.route('/results/<key>')
def results(key):
    """Renders the results page."""
    poll = repository.get_poll(key)
    poll.calculate_stats()
    return render_template(
        'results.html',
        title='Results',
        year=datetime.now().year,
        poll=poll,
    )

@app.route('/poll/<key>', methods=['GET', 'POST'])
def details(key):
    """Renders the poll details page."""
    error_message = ''
    if request.method == 'POST':
        try:
            choice_key = request.form['choice']
            repository.increment_vote(key, choice_key)
            return redirect('/results/{0}'.format(key))
        except KeyError:
            error_message = 'Please make a selection.'

    return render_template(
        'details.html',
        title='Poll',
        year=datetime.now().year,
        poll=repository.get_poll(key),
        error_message=error_message,
    )

# @app.errorhandler(PollNotFound)
# def page_not_found(error):
#     """Renders error page."""
#     return 'Poll does not exist.', 404
