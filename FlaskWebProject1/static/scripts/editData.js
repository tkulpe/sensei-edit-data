var PLOTDATA = PLOTDATA || (function() {

    return {
        plot: function(args) {

            console.log(args[0]);

            var datas = JSON.parse(args[0]);

            var palette = new Rickshaw.Color.Palette();

            var graphs = [];

            var width = 800,
                height = 200;

            datas.forEach(dataElement => {
                var pointArry = [];

                for (i = 0; i < dataElement.length; i++) {
                    if (i % 50 != 0)
                        continue;
                    pointArry.push({
                        x: i,
                        y: dataElement[i]
                    });
                }

                var graph = new Rickshaw.Graph({
                    element: document.getElementById("chart"),
                    width: width,
                    height: height,
                    stroke: true,
                    preserve: true,
                    renderer: 'line',
                    series: [{
                        name: "DATA",
                        data: pointArry,
                        color: 'black',
                    }],
                });

                graph.render();

                //var x_axis = new Rickshaw.Graph.Axis.X({
                //    graph: graph,
                //    width: width,
                //    orientation: 'bot'
                //});

                //x_axis.render();

                var y_axis = new Rickshaw.Graph.Axis.Y({
                    graph: graph,
                    height: height,
                    orientation: 'left',
                    element: document.getElementById('y_axis'),
                });

                y_axis.render();

                graphs.push(graph);
            });

            var preview = new Rickshaw.Graph.RangeSlider.Preview({
                graphs: graphs,
                element: document.getElementById('preview'),
            });

            var previewXAxis = new Rickshaw.Graph.Axis.Time({
                graphs: preview.previews,
                timeFixture: new Rickshaw.Fixtures.Time.Local(),
                ticksTreatment: ticksTreatment
            });

            previewXAxis.render();
            var x1, x2;
            //var domain = graph.dataDomain();

            preview.RangeSlider.onUpdate(function() {
                x1 = preview.RangeSlider.xMin;
                x2 = preview.RangeSlider.xMax;
                console.log(x1 + " - " + x2 + "; ");
            });
        }
    };
}());