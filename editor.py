# import the library
import matplotlib.pyplot as plt
import tkinter as tk
import numpy as np

def readfile(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
        y = [int(item[1].split(';')[0]) for item in enumerate(lines) if item[0] > 2]
        return y


class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.createWidgets()

    def createWidgets(self):

        self.plot = plt.plot(readfile('data.csv'))

        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.QUIT = tk.Button(self, text="QUIT", fg="red",
                                            command=root.destroy)
        self.QUIT.pack(side="bottom")

    def say_hi(self):
        print("hi there, everyone!")

root = tk.Tk()
app = Application(master=root)
app.mainloop()

#
# plt.ylabel('some numbers')
# app.(plt)




